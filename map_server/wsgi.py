
import os
import sys

from os.path import abspath, dirname, join

sys.path.append(abspath(dirname(dirname(dirname(__file__)))))
sys.path.append(abspath(join(dirname(__file__), "../env/lib/python2.7/site-packages/")))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "map_server.settings")

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
