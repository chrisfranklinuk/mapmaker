# MIDDLEWARE_CLASSES and INSTALLED_APPS keys are merged

DEBUG = True
TEMPLATE_DEBUG = DEBUG

from os.path import abspath, dirname, join
SITE_ROOT = abspath(dirname(dirname(dirname(__file__))))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(SITE_ROOT, 'db/db.sqlite')
    }
}


# Debug & DebugToolbar #################################################################################################
MIDDLEWARE_CLASSES = ('debug_toolbar.middleware.DebugToolbarMiddleware', )
INSTALLED_APPS = ('debug_toolbar', )


def debug_only_check(request):
    if DEBUG:
        return True
    return False

DEBUG_TOOLBAR_CONFIG = {'SHOW_TOOLBAR_CALLBACK': debug_only_check, 'INTERCEPT_REDIRECTS': False,}


# Map Publishing #######################################################################################################
MAP_PUBLISH_ROOT_URL = "http://localhost:8000/maps/maps/"



# Logging ##############################################################################################################
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'},
    },
    'handlers': {
        'default':
            {'level': 'DEBUG',
             'class': 'logging.handlers.RotatingFileHandler',
             'filename': join(SITE_ROOT, 'logs/site.log'),
             'maxBytes': 1024 * 1024 * 1, # 1 MB
             'backupCount': 0,
             'formatter': 'verbose',
            },
        'request_handler':
            {'level': 'DEBUG',
             'class': 'logging.handlers.RotatingFileHandler',
             'filename': join(SITE_ROOT, 'logs/request.log'),
             'maxBytes': 1024 * 1024 * 1,
             'backupCount': 0,
             'formatter': 'verbose',
            },
    },
    'loggers': {
        '':
            {'handlers': ['default'],
             'level': 'DEBUG',
             'propagate': True
            },
        'django.request': # Stop SQL debug from logging to main logger
            {'handlers': ['request_handler'],
             'level': 'WARNING',
             'propagate': False
            },
        'django.db.backends': # Stop SQL debug from logging to main logger
            {'handlers': ['request_handler'],
             'level': 'WARNING',
             'propagate': False
            },
    }
}